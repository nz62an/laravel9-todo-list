@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        ویرایش تسک
                    </div>
                    <div class="card-body">
                        <form action="{{ route('todos.update',['todo'=>$todo->id]) }}" method="post">
                            @csrf
                            @method('put')
                            <div class="mb-3">
                                <label class="form-label" for="title">نام</label>
                                <input class="form-control  @error('title')  is-invalid  @enderror" type="text" name="title" id="title" value="{{ $todo->title }}">
                                @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="description">توضیحات</label>
                                <textarea class="form-control @error('description')  is-invalid  @enderror" name="description" id="description" rows="4">{{ $todo->description }}</textarea>
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <button class="btn btn-info" type="submit">تأیید</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
