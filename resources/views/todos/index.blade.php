@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="d-flex justify-content-between my-3">
                    <h4>لیست تسک ها</h4>
                    <a class="btn btn-outline-success" href="{{ route('todos.create') }}">تسک جدید</a>
                </div>
                <div class="card">
                    <div class="card-header">
                        تسک ها
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            @foreach ($todos as $todo)
                                <li class="list-group-item card-title d-flex justify-content-between">{{ $todo->title }}
                                    <div>
                                        <a class="btn btn-primary"
                                            href="{{ route('todos.show', ['todo' => $todo->id]) }}">نمایش</a>
                                        @if (!$todo->completed)
                                            <a class="btn btn-secondary"
                                                href="{{ route('todos.complete', ['todo' => $todo->id]) }}">انجام شد</a>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div class="d-flex justify-content-center">
                            {{ $todos->onEachSide(2)->links() }}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
