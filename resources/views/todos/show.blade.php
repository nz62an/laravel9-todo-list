@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h4 class="text-center mt-5 mb-3"> {{$todo->title}}</h4>
                <div class="card">
                    <div class="card-header">
                        توضیحات
                    </div>
                    <div class="card-body">
                          <span>{{$todo->description}}</span>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex">
                            <a class="btn btn-success" href="{{ route('todos.edit', ['todo'=>$todo->id]) }}">ویرایش</a>
                            <form action="{{ route('todos.destroy', ['todo'=>$todo->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger me-2" type="submit">حذف</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
