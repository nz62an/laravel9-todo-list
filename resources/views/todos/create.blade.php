@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        تسک جدید
                    </div>
                    <div class="card-body">
                        <form action="{{ route('todos.store') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label class="form-label" for="title">نام</label>
                                <input class="form-control  @error('title')  is-invalid  @enderror" type="text" name="title" id="title" value="{{ old('title') }}">
                                @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="description">توضیحات</label>
                                <textarea class="form-control @error('description')  is-invalid  @enderror" name="description" id="description" rows="4">{{ old('description') }}</textarea>
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <button class="btn btn-info" type="submit">ذخیره</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
